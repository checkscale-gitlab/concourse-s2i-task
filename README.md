# concourse-s2i-task
A Concourse s2i task to create a Dockerfile with [s2i](https://github.com/openshift/source-to-image).
[![pipeline status](https://gitlab.com/ibotty/concourse-s2i-task/badges/master/pipeline.svg)](https://gitlab.com/ibotty/concourse-s2i-task/commits/master)



## Usage

Until concourse implements [reusable tasks](https://github.com/concourse/rfcs/issues/7) the configuration has to be manual.

Unfortunately that means that the only way to configure it is via environment variables.

The following environment variables influence the task.

 * `BUILDER_IMAGE`, required, the s2i image to use,
 * `SOURCE_PATH`, the source resource path, defaults to `source`,
 * `BUILD_PATH`, the build resource path, defaults to `build`,
 * `BUILDER_S2I_SCRIPTS_URL`, if the s2i scripts url has to be overwritten (see [this bug](https://github.com/openshift/source-to-image/issues/973))

```yaml
resources:
- get: my-image-source
  type: git
  source:
    uri: https://github.com/...

- name: my-image
  type: docker-image
  source:
    repository: my-user/my-repo

jobs:
- name: build-and-push
  plan:
  # fetch repository source (containing Dockerfile)
  - get: my-image-source

  - task: source for Dockerfile build
    config:
      platform: linux
      image_resource:
        type: docker-image
        source:
          repository: registry.gitlab.com/ibotty/concourse-s2i-task
      params:
        BUILD_PATH: dockerbuild-source
        BUILDER_IMAGE: registry.access.redhat.com/ubi8/php-72
        SOURCE_PATH: build-website
      run:
        path: build
      inputs:
      - name: my-image-source
      outputs:
      - name: dockerbuild-source

  - put: my-image
    params:
      build: dockerbuild-source
